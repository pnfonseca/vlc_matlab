function rel_p = pd_recpower2d( P, d, psi, psi_c)
%rel_p = pd_recpower2d( P, d, psi, psi_c)
%
%pd_recpower2d Compute the power received by a photo-diode
%   Computes the power received by a photo-diode (PD) 
%   based on the distance to the receiver and the
%   angle psi


    rel_p = P /(d^2) * s_sfh213(psi) .*  (abs(psi) <= psi_c);
    % rel_p = P /(d^2) * cos(psi)^10 .*  (abs(psi) <= psi_c);

return  

end


