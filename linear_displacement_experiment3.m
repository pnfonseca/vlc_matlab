% Simulation of photodiode arc sensor received power
%
% 3 light sources
%
% PF, 23.06.2015

close all;

%% Sensitivity function

% This experiment considers a SFH213 photodiode.
% Plot the receiving sensitivity function
% 
% See http://www.osram-os.com/Graphics/XPic5/00101689_0.pdf, page 6
% for the sensitivity function graph

angles=-pi:pi/60:pi;

fig1 = figure;

plot(angles*180/pi,s_sfh213(angles));
grid on;
title('Sensitivity function of the PD');
ylabel('Sensitivity');
xlabel('Angle (in degrees');
axis([0 +120 -0.2 1.1]);
ax = gca;
ax.YTick = -0.2:.1:1.1;
ax.XTick = 0:10:120;

%% Experiments results


% array with x coords
xpos= 0:0.1:3;  

% array of photodiode angles
% These are the orientation angles for the photodiodes in the 
% receiver
pd_angles = (180 - [ 90 79.1 68.35 57.79 47.45 37.38 27.65 18.41]-2)*pi/180;

% Emitter position
xe_values = [0 0.07 -0.07];    

% The emitter is at 2.18m
ye = 2.08; 

% photodiode FOV (one-sided)
psi_c = (7.4/2)*pi/180;

% External radius of the sensor structure
R = 0.124;

final = [];
for xe = xe_values
    % Compute the received values, for each diode and for each position
    res = [];
    for angle=pd_angles
        res_line = [];
        % x_pd and y_pd are the x and y coords of the PD, relative to
        % the sensor structure center
        x_pd = R * cos(angle);
        y_pd = R * sin(angle);
        for x = xpos
            res_line = [ res_line pd_receive2d(x+x_pd,y_pd,angle,psi_c,xe,ye,1)];
        end
        res = [ res ; res_line ];
    end
    % store the result
    
    if size(final) == [0 0]
        final = [res];
    else
        final = final + res;
    end
end




% Draw the results
fig_results = figure;
hold

% Plot the results for each photodiode
for i=1:size(final,1)
    plot(xpos,final(i,:),'.-')
end

title('Received power at the photodiodes (simulated)')
ylabel('Received power');
xlabel('x offset from source (in meters)');

%% Comparação com dados experimentais

% Dados recolhidos pelo Filipe Duarte

dados_experimentais = csvread('dados_filipe_duarte.csv',1,1);
x_dist = csvread('dados_filipe_duarte.csv',0,1,[0 1 0 31])/100;

fig_exp = figure;
hold

% Plot the results for each photodiode
for i=1:size(dados_experimentais,1)
    plot(x_dist,dados_experimentais(i,:),'.-')
end

title('Received signal amplitude at the photodiodes (experimental)')
ylabel('Received signal amplitude');
xlabel('x offset from source (in meters)');
