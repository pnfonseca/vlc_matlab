function S = fteste(alpha)

beta = interp1(pi/180*[0 5 10 60 90],pi/180*[0 15 45 89.9 90], alpha, 'pchip');

S = 0.5*(cos(2*beta)+1);