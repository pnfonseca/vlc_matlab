function S = s_sfh213(alpha)
% function S = s_sfh213(alpha)
% 
% Receiving sensibility function for a SFH213 photodiode

% Normalize the angle to -pi:pi
alpha = alpha - 2*pi*floor( (alpha+pi)/(2*pi) );

% The sensitivity function is simetrical wrt the main axis. 
% The interpolation function is defined for positive values only
% Only positive angle values are used in the calculations. 
alpha = abs(alpha);

% beta is the result of the abscissa's change of coordinates to use a 
% cossine function to compute the sensibility.
beta = interp1(pi/180*[0 5 10 60 90],pi/180*[0 15 45 89.9 90], alpha, 'pchip');

S = 0.5*(cos(2*beta)+1);