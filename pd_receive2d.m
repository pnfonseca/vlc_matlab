function rel_p = pd_receive2d ( x, y, alpha, psi_c, xe, ye, Pe)
%[ rel_p ] = pd_receive2d ( x, y, alpha, psi_c, xe, ye, Pe)
%
%PD_receive Compute the power received by a photo-diode
%   Computes the power received by a photo-diode (PD) 
%   given the receiver position, heading and field-of-view and the
%   emitter's position and radiating power. 
%
% The model for the receiver considers: 
% - positioned at location (x,y) in the plane
% - oriented at an angle alpha from the x direction
% - a viewing angle of psi_c
%
% The emitter: 
% - is located at (xe, ye)
% - broadcasts with a power Pe 

% compute distance vector from receiver to emitter:
v = [ xe, ye ] - [x y ];

% compute distance 
d = norm(v);

% compute angular position of emitter rel. to receiver heading
beta = atan2(v(2), v(1));
psi = beta - alpha;

% compute received power
rel_p = pd_recpower2d ( Pe, d, psi, psi_c);

return  

end